import java.time.LocalDate;
import java.util.Scanner;

///////////////////////////////////////////////////////////////////////////
// An Administration instance needs a User as input, which is passed via the
// constructor to the data member 'çurrentUser'.
// The patient data is available via the data member çurrentPatient.
/////////////////////////////////////////////////////////////////
class Administration
{
   static final int STOP = 0;
   static final int VIEW = 1;
   static final int CHOOSEPATIENT = 2;
   static final int CHOOSEUSER = 3;
   static final int EDITPATIENTDATA = 4;
   static final int SHOWMEDLIST = 5;
   static final int ADDMEDS = 6;
   static final int REMOVEMEDS = 7;


   Patient currentPatient;            // The currently selected patient
   User    currentUser;               // the current user of the program.

   /////////////////////////////////////////////////////////////////
   // Constructor
   /////////////////////////////////////////////////////////////////
   Administration( User user )
   {
      currentUser    = user;
      currentPatient = new Patient(1, "Bergamin", "Lucca-Jan", LocalDate.of(2000,9,9), 1.82, 63.0);
   }

   /////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////
   void menu()
   {
      var scanner = new Scanner( System.in );  // User input via this scanner.
      boolean nextCycle = true;
      while (nextCycle)
      {
         System.out.println();
         System.out.println("=".repeat(30) + "   Main Menu   " + "=".repeat(30));
         System.out.println("Current user: #" + currentUser.getUserID() + " " + currentUser.getUserName() + " [" + currentUser.getUserDiscipline() + "]");
         System.out.println("Current patient: " + currentPatient.fullName());
         System.out.println("-----------------------------------------------");

         ////////////////////////
         // Print menu on screen
         ////////////////////////
         System.out.format( "%d:  STOP\n", STOP );
         System.out.format( "%d:  View patient data\n", VIEW );
         System.out.format( "%d:  Choose current patient \n", CHOOSEPATIENT );
         System.out.format( "%d:  Choose current user \n", CHOOSEUSER );
         System.out.format( "%d:  Edit Patient Data \n", EDITPATIENTDATA );
         if (currentUser.showMedListRights()) {
            System.out.format( "%d:  Show Medicine List \n", SHOWMEDLIST );
         }
         if (currentUser.viewRightsAddMed()) {
            System.out.format("%d:  Add Medicine to Patient \n", ADDMEDS);
         }
         if (currentUser.viewRightsRemoveMed()) {
            System.out.format("%d:  Remove Medicine from Patient \n", REMOVEMEDS);
         }
         System.out.println();

         ////////////////////////

         System.out.print("Enter choice: ");
         int choice = scanner.nextInt();
         switch (choice)
         {
            case STOP: // interrupt the loop
               System.out.println();
               System.out.println("----- Fijne dag nog! -----");
               nextCycle = false;
               break;

            case VIEW:
               currentPatient.viewData(currentUser);
               break;

            case CHOOSEPATIENT:
               currentPatient = currentPatient.choosePatient();
               break;

            case CHOOSEUSER:
               currentUser = currentUser.chooseUser();
               break;

            case EDITPATIENTDATA:
               currentPatient.editPatientData(currentUser);
               break;

            case SHOWMEDLIST:
               MedicineFunctions.showMedList();
               break;

            case ADDMEDS:
               MedicineFunctions.addMeds();
               break;

            case REMOVEMEDS:
               MedicineFunctions.removeMeds();
               break;

            default:
               System.out.println( "Please enter a *valid* digit" );
               break;
         }
      }
   }
}
