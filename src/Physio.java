class Physio extends User {

    public Physio(int id, String name, String discipline) {
        super(id, name, discipline);
    }

    public String getDiscipline(){
        System.out.println("This User is a Physiotherapist");
        return "Physiotherapist";
    }

    @Override public boolean viewRightsAddMed() {
        return false;
    }

    @Override public boolean viewRightsRemoveMed() {
        return false;
    }

    @Override public boolean showLengthRights() {
        return true;
    }

    @Override public boolean showWeightRights() {
        return true;
    }

    @Override public boolean showMedListRights() {
        return true;
    }

    @Override public boolean showPersonalMedListRights() {
        return true;
    }



}