import javax.print.attribute.standard.MediaName;
import java.util.ArrayList;
import java.util.Scanner;

public class MedicineFunctions {
    public static ArrayList<Medicine> patientMedArrayList = new ArrayList<>();

    // Functie die de Medicine ArrayList laat zien
    public static void showMedList() {
        ArrayList<Medicine> medicineArrayList = new ArrayList<>();
        medicineArrayList.add(new Medicine(1, "Ibuprofen        ", "Pill       ", 60));
        medicineArrayList.add(new Medicine(2, "Ureum            ", "Zalf       ", 230));
        medicineArrayList.add(new Medicine(3, "Amylmetacresol   ", "Zuigtablet ", 420));
        medicineArrayList.add(new Medicine(4, "Glycolsalicylaat ", "Zalf       ", 300));

        System.out.println();
        System.out.println("  ========================= Medicine List =========================");
        for (Medicine brock : medicineArrayList) {
            System.out.println("ID: " + brock.getMedID() + ",   Name: " + brock.getMedName() + "   Type:  " + brock.getMedType() + "   Dosage: " + brock.getMedDosage() + " mg");
        }
    }

    // Functie die Meds6 kan toevoegen aan de Patient
    public static void addMeds() {
        ArrayList<Medicine> addmedicineArrayList = new ArrayList<>();
        addmedicineArrayList.add(new Medicine(1, "Ibuprofen        ", "Pill       ", 60));
        addmedicineArrayList.add(new Medicine(2, "Ureum            ", "Zalf       ", 230));
        addmedicineArrayList.add(new Medicine(3, "Amylmetacresol   ", "Zuigtablet ", 420));
        addmedicineArrayList.add(new Medicine(4, "Glycolsalicylaat ", "Zalf       ", 300));

        // TODO: een nummering aan de lijst toevoegen, is mooi
        System.out.println();
        System.out.format("%s\n", "=".repeat(80));
        System.out.println(" ".repeat(25) + "Choose a medicine to add to the patient:" + " ".repeat(25));
        System.out.println();
        System.out.println("0: Return to Main Menu");
        for (Medicine adder : addmedicineArrayList) {
            System.out.println("ID: " + adder.getMedID() + ",   Name: " + adder.getMedName() + "   Type:  " + adder.getMedType() + "   Dosage: " + adder.getMedDosage() + " mg");
        }
        var scanner = new Scanner(System.in);
        System.out.println();
        System.out.print("Choose ID number: ");
        int medAddChoice = scanner.nextInt();

        switch (medAddChoice) {
            case 0:
                System.out.println(" ".repeat(27) + "Returning to Main Menu!");
                break;

            case 1:
                patientMedArrayList.add(new Medicine(1, "Ibuprofen        ", "Pill       ", 60));
                System.out.println(" ".repeat(27) + "Medicine Added!");
                break;

            case 2:
                patientMedArrayList.add(new Medicine(2, "Ureum            ", "Zalf       ", 230));
                System.out.println(" ".repeat(27) + "Medicine Added!");
                break;

            case 3:
                patientMedArrayList.add(new Medicine(3, "Amylmetacresol   ", "Zuigtablet ", 420));
                System.out.println(" ".repeat(27) + "Medicine Added!");
                break;

            case 4:
                patientMedArrayList.add(new Medicine(4, "Glycolsalicylaat ", "Zalf       ", 300));
                System.out.println(" ".repeat(27) + "Medicine Added!");
                break;

            default:
                System.out.println();
                System.out.println("              Oops, wrong number! Returning to Main Menu.");
                break;

        }
    }

    // Functie die Meds kan verwijderen van een Patient
    public static void removeMeds() {
        int positionNumber = 1;
        System.out.format("%s\n", "=".repeat(80));
        System.out.println(" ".repeat(22) + "Choose a medicine to remove from the patient:" + " ".repeat(25));
        System.out.println();
        System.out.println("0:  Return to Main Menu");
        System.out.println();
        for (Medicine medicine : MedicineFunctions.patientMedArrayList) {
            System.out.println("---------- Medicine Number: " + positionNumber++ + " ----------");
            System.out.println("Name: " + medicine.getMedName());
            System.out.println("Type:  " + medicine.getMedType());
            System.out.println("Dosage: " + medicine.getMedDosage() + " mg");
            System.out.println();
        }
        var scanner = new Scanner(System.in);
        System.out.println();
        System.out.print("Enter Medicine Number: ");
        int removeChoice = scanner.nextInt();

        if (removeChoice == 0) {
            return;
        } else {
            int finalRemoveChoice = removeChoice - 1;
            MedicineFunctions.patientMedArrayList.remove(finalRemoveChoice);
        }
    }


    //TODO: een editDosage functie maken
}
