class ZorgApp
{
   public static void main( String[] args )
   {
      User           user           = new User (3,"Harry Huisarts", "General Practitioner");
      Administration administration = new Administration( user );

      administration.menu();
   }
}
