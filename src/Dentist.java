class Dentist extends User {

    public Dentist(int id, String name, String discipline) {
        super(id, name, discipline);
    }

    public String getDiscipline(){
        System.out.println("This User is a Dentist");
        return "Dentist";
    }

    @Override public boolean viewRightsAddMed() {
        return false;
    }

    @Override public boolean viewRightsRemoveMed() {
        return false;
    }

    @Override public boolean showLengthRights() {
        return false;
    }

    @Override public boolean showWeightRights() {
        return false;
    }

    @Override public boolean showMedListRights() {
        return false;
    }

    @Override public boolean showPersonalMedListRights() {
        return false;
    }

}