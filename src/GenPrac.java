class GenPrac extends User {

    public GenPrac(int id, String name, String discipline) {
        super(id, name, discipline);
    }

    public String getDiscipline(){
        System.out.println("This User is a General Practitioner");
        return "General Practitioner";
    }

    @Override public boolean viewRightsAddMed() {
        return true;
    }

    @Override public boolean viewRightsRemoveMed() {
        return true;
    }

    @Override public boolean showLengthRights() {
        return true;
    }

    @Override public boolean showWeightRights() {
        return true;
    }

    @Override public boolean showMedListRights() {
        return true;
    }

    @Override public boolean showPersonalMedListRights() {
        return true;
    }

}