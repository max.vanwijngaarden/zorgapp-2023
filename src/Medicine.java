import java.util.ArrayList;

public class Medicine {

        // Defines Medicine
    private int  medID;
    private String  medName;
    private String  medType;
    private int     medDosage;


        // Medicine Getters
    public int      getMedID() {return medID;}
    public String   getMedName() {return medName;}
    public String   getMedType() {return medType;}
    public  int     getMedDosage() {return medDosage;}

        // Medicine Setter(s)
    public void setMedID(int medID) {this.medID = medID;}
    public void setMedName(String medName) {this.medName = medName;}
    public void setMedType(String medType) {this.medType = medType;}
    public void setMedDosage(int medDosage) {this.medDosage = medDosage;}


        // Constructor
    public Medicine (int medId, String medName, String medType, int medDosage) {
        this.medID      = medId;
        this.medName    = medName;
        this.medType    = medType;
        this.medDosage  = medDosage;
    }
}
