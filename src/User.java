import java.util.ArrayList;
import java.util.Scanner;

class User
{
   String userName;
   int    userID;
   String userDiscipline;

   /// Getters
   String getUserName() {
      return userName;
   }
   int getUserID() {
      return userID;
   }

   String getUserDiscipline() {
      return userDiscipline;
   }

   public User(int id, String name, String discipline)
   {
      this.userID          =  id;
      this.userName        =  name;
      this.userDiscipline  =  discipline;
   }


   /// Functie om een nieuwe current user te kiezen
   public User chooseUser() {
      ArrayList<User> userArrayList = new ArrayList<>();
      userArrayList.add(new Dentist(1,"Timmy Tandarts", "Dentist"));
      userArrayList.add(new Physio(2,"Fleur Fysio", "Physiotherapist"));
      userArrayList.add(new GenPrac(3,"Harry Huisarts", "General Practitioner"));
      System.out.format( "%s\n", "=".repeat( 80 ) );
      System.out.println(" ".repeat(25) + "Choose a new User:" + " ".repeat(25));
      System.out.println();
      for (User peter : userArrayList) {
         System.out.println(peter.getUserID() + ":  " + peter.getUserName());
      }
      var scanner = new Scanner(System.in);
      System.out.print("Enter choice: ");
      int userchoice = scanner.nextInt();
      int finalUserChoice = userchoice - 1;
      if (finalUserChoice >2) {
         System.out.println();
         System.out.println("              Oops, wrong number! Switching to Default User.");
         return new User(0, "Urnie User", "Default User");
      }
      System.out.println(" ".repeat(23) + "Current User Updated!");
      return userArrayList.get(finalUserChoice);
   }

   public String getDiscipline(){
      System.out.println("This User is a Default User");
      return "Default User";
    }

   public boolean viewRightsAddMed() {
      return true;
   }

   public boolean viewRightsRemoveMed() {
      return true;
   }

   public boolean showLengthRights() {
      return true;
   }

   public boolean showWeightRights() {
      return true;
   }

   public boolean showMedListRights() {
      return true;
   }

   public boolean showPersonalMedListRights() {
      return true;
   }

}
