/// Imported Libraries
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Scanner;

class Patient
{
   /// Menu Tags
   private static final int RETURN      = 0;
   private static final int SURNAME     = 1;
   private static final int FIRSTNAME   = 2;
   private static final int DATEOFBIRTH = 3;
   private static final int AGE         = 4;
   private static final int LENGTH      = 5;
   private static final int WEIGHT      = 6;
   private static final int BMI         = 7;


   /// Establish Variables of Patient
   private int       id;
   private String    surname;
   private String    firstName;
   private LocalDate dateOfBirth;
   private Period    ageInYears;
   private double    length;
   private double    weight;
   private double    bmi;
   private Medicine  meds;

   /// Getters
   public int getId() {return id;}
   public String getSurname() {return surname;}
   public String getFirstName() {return firstName;}
   public LocalDate getDateOfBirth() {return dateOfBirth;}
   public double getLength() {return length;}
   public double getWeight() {return weight;}
   public Medicine getMeds() {return meds;}

   /// Setters
   public void setSurname(String newSurname) {
      this.surname = newSurname;
   }
   public void setFirstname(String newFirstname) {
      this.firstName = newFirstname;
   }
   public void setDateOfBirth(LocalDate newDateOfBirth) {
      this.dateOfBirth = newDateOfBirth;
   }
   public void setLength(double newLength) {
      this.length = newLength;
   }
   public void setWeight(double newWeight) {
      this.weight = newWeight;
   }

   /// Constructor
   Patient( int id, String surname, String firstName, LocalDate dateOfBirth, double length, double weight)
   {
      this.id          = id;
      this.surname     = surname;
      this.firstName   = firstName;
      this.dateOfBirth = dateOfBirth;
      this.ageInYears  = Period.ofYears(Period.between(dateOfBirth,LocalDate.now()).getYears());
      this.length      = length;
      this.weight      = weight;
      this.bmi         = weight/(length*length);
   }

   /// Display patient data.
   void viewData(User currentUser) {
      int positionNumber = 1;
      System.out.println();
      System.out.format("========================= Patient ID=%d =========================\n", id);
      System.out.format("%-17s %s\n", "Surname:", surname);
      System.out.format("%-17s %s\n", "firstName:", firstName);
      System.out.format("%-17s %s\n", "Date of birth:", dateOfBirth);
      System.out.format("%-17s %s\n", "Age in Years:", ageInYears.getYears());
      if (currentUser.showLengthRights()) {
         System.out.format("%-17s %s\n", "Length:", length, "meter");
      }
      if (currentUser.showWeightRights()) {
         System.out.format("%-17s %s\n", "Weight:", weight, "kg");
      }
      if (currentUser.showWeightRights()) {
         System.out.format("%-17s %.2f\n", "BMI:", bmi);
      }
      if (currentUser.showPersonalMedListRights()) {
         System.out.format("============= Medicine List: =============\n", id);
         for (Medicine connor : MedicineFunctions.patientMedArrayList) {
            System.out.println();
            System.out.println("---------- Medicine Number: " + positionNumber++ + " ----------");
            System.out.println("Name: " + connor.getMedName());
            System.out.println("Type:  " + connor.getMedType());
            System.out.println("Dosage: " + connor.getMedDosage() + " mg");
         }
      }
   }

   /// Functie om een nieuwe current patient te kiezen
   public Patient choosePatient() {
      ArrayList<Patient>patientArrayList= new ArrayList<>();
      patientArrayList.add(new Patient(1, "Bergamin", "Lucca-Jan", LocalDate.of(2000,9,9), 1.82, 63.0));
      patientArrayList.add(new Patient(2, "Straatman", "Senne", LocalDate.of(1999,9,30), 1.82, 75.0));
      patientArrayList.add(new Patient(3, "Altena", "Madeleine", LocalDate.of(2002,11,6), 1.75, 65.0));
      patientArrayList.add(new Patient(4, "van Kleef", "Jonas", LocalDate.of(2002,2,22), 1.92, 70.0));
      patientArrayList.add(new Patient(5, "Beekmans", "Loes", LocalDate.of(2002,4,2), 1.63, 52.3));
      patientArrayList.add(new Patient(6, "van Bier", "Max", LocalDate.of(2001,12,14), 1.77, 62.1));
      patientArrayList.add(new Patient(7, "van Berkel", "Isabella", LocalDate.of(2002,7,4), 1.80, 70.0));

      System.out.println();
      System.out.format( "%s\n", "=".repeat( 80 ) );
      System.out.println(" ".repeat(25) + "Choose a new current patient:" + " ".repeat(25));
      System.out.println();
      for (Patient sarah : patientArrayList) {
         System.out.println(sarah.getId() + ":  " + sarah.getFirstName() + " " + sarah.getSurname());
      }
      var scanner = new Scanner(System.in);
      System.out.print("Enter choice: ");
      int patientChoice = scanner.nextInt();
      int finalPatientChoice = patientChoice - 1;
      if (finalPatientChoice >6) {
         System.out.println();
         System.out.println("              Oops, wrong number! Switching to Default Patient.");
         return new Patient(0, "Doe", "John", LocalDate.of(2000,1,1), 1.80, 69.0);
      }
      System.out.println(" ".repeat(27) + "Current Patient Updated!");
      return patientArrayList.get(finalPatientChoice);
   }


   /// Functie die de Patient data kan bewerken
   public Patient editPatientData(User currentUser) {
      boolean editLoop = true;
      while (editLoop = true) {
         System.out.format("%s\n", "=".repeat(80));
         System.out.println(" ".repeat(25) + "Choose a variable to change:" + " ".repeat(25));
         System.out.println();
         System.out.println("0:  Return to Main Menu");
         System.out.println("1:  First Name      " + "( " + getFirstName() + " )");
         System.out.println("2:  Surname         " + "( " + getSurname() + " )");
         System.out.println("3:  Date of Birth   " + "( " + getDateOfBirth() + " )");
         if (currentUser.showLengthRights()) {
            System.out.println("4:  Length          " + "( " + getLength() + " )");
         }
         if (currentUser.showWeightRights()) {
            System.out.println("5:  Weight          " + "( " + getWeight() + " )");
         }
         var scanner = new Scanner(System.in);
         System.out.print("Enter choice: ");
         int editChoice = scanner.nextInt();

         switch (editChoice) {
            case 0:
               return null;

            case 1:
               System.out.println("Choose new value for First Name:");
               String editFirstName = scanner.next();
               setFirstname(editFirstName);
               System.out.println();
               System.out.println(" ".repeat(27) + "First Name Updated!");
               break;

            case 2:
               System.out.println("Choose new value for Surname:");
               String editSurname = scanner.next();
               setSurname(editSurname);
               System.out.println();
               System.out.println(" ".repeat(27) + "Surname Updated!");
            break;

            case 3:
               System.out.println("Choose new value for Date of Birth :");
               System.out.println("Write as: 'yyyy-mm-dd'");
               String editDateOfBirth = scanner.next();
               setDateOfBirth(LocalDate.parse(editDateOfBirth));
               System.out.println();
               System.out.println(" ".repeat(27) + "Date of Birth Updated!");
            break;

            case 4:
               System.out.println("Choose new value for Length:");
               System.out.println("Write as: '0.00'");
               String editLength = scanner.next();
               setLength(Double.parseDouble(editLength));
               System.out.println();
               System.out.println(" ".repeat(27) + "Length Updated!");
            break;

            case 5:
               System.out.println("Choose new value for Length:");
               System.out.println("Write as: '0.0'");
               String editWeight = scanner.next();
               setWeight(Double.parseDouble(editWeight));
               System.out.println();
               System.out.println(" ".repeat(27) + "Weight Updated!");
            break;

            default:
               return null;
         }
      }
      return null;
   }


   /// Shorthand for a Patient's full name
   String fullName()
   {
      return String.format( "%s %s [%s]", firstName, surname, dateOfBirth.toString() );
   }
}
